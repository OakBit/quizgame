﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace QuizGame
{
    public class Question
    {
        static Random rand = new Random();

        private int minNumber = 0;
        private int maxNumber = 100;

        private bool Saved;

        private string questionString;
        public int Answer { get; }
        private int Guess;
        public int SavedGuess { get { return Guess; } }

        public Question(int difficulty)
        {

            switch (difficulty)
            {
                //Keeping it simple, easy is addition and subtraction, medium is multiply, hard is all in one
                case 1:

                    questionString += rand.Next(minNumber, maxNumber).ToString();
                    switch (rand.Next(1, 2))
                    {
                        case 1:
                            questionString += " + ";
                            break;
                        case 2:
                            questionString += " - ";
                            break;
                    }
                    questionString += rand.Next(minNumber, maxNumber).ToString();

                    break;
                case 2:
                    questionString += rand.Next(minNumber, maxNumber).ToString();

                    questionString += " * ";

                    questionString += rand.Next(minNumber, maxNumber).ToString();

                    break;
                case 3:

                    questionString += rand.Next(minNumber, maxNumber).ToString();
                    switch (rand.Next(1, 2))
                    {
                        case 1:
                            questionString += " + ";
                            break;
                        case 2:
                            questionString += " - ";
                            break;
                    }

                    questionString += rand.Next(minNumber, maxNumber).ToString();

                    questionString += " * ";

                    questionString += rand.Next(minNumber, maxNumber).ToString();

                    break;
            }

            Answer = Convert.ToInt32(new DataTable().Compute(questionString, null));
        }

        public bool IsSaved { get { return Saved; } }

        public bool CheckGuess()
        {
            if (Guess == Answer)
            {
                return true;
            }
            return false;
        }

        public void Save(int guess)
        {
            Guess = guess;
            Saved = true;
        }

        public void Cancel()
        {
            //Not the best way, may be cases where answer is 0
            Guess = 0;
            Saved = false;
        }

        public override string ToString()
        {
            return questionString;
        }
    }
}
