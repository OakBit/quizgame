﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace QuizGame
{
    /// <summary>
    /// Interaction logic for EntryWindow.xaml
    /// </summary>
    /// 
    public enum Diff
    {
        Easy = 1,
        Medium = 2,
        Hard = 3
    }
    public partial class EntryWindow : Window
    {
        /*********************************************
        PROPERTIES
        *********************************************/
        public ObservableCollection<int> lNumQuestions;


        /*********************************************
        LOAD
        *********************************************/
        public EntryWindow()
        {
            InitializeComponent();

            this.Loaded += EntryWindow_Loaded;
        }

        private void EntryWindow_Loaded(object sender, RoutedEventArgs e)
        {
            LoadComponents();

            this.DataContext = this;
        }

        void LoadComponents()
        {
            lNumQuestions = new ObservableCollection<int>();
            for(int i = 5; i < 21; i+=5)
            {
                lNumQuestions.Add(i);
            }
            cboNumQuestions.ItemsSource = lNumQuestions;
            
        }

        /*********************************************
        EVENTS
        *********************************************/
        private void btnContinue_Click(object sender, RoutedEventArgs e)
        {
            if (Validate())
            {
                App.Current.UserName = tbName.Text;
                App.Current.NumQuestions = (int)cboNumQuestions.SelectedValue;
                if (rbEasy.IsChecked == true)
                    App.Current.Difficulty = (int) Diff.Easy;
                if (rbMedium.IsChecked == true)
                    App.Current.Difficulty = (int)Diff.Medium;
                if (rbHard.IsChecked == true)
                    App.Current.Difficulty = (int)Diff.Hard;

                MainWindow w = new MainWindow();
                w.Show();
                this.Close();
            }
        }

        /*********************************************
        UTILITY
        *********************************************/
        bool Validate()
        {
            if (tbName.Text == "")
                return false;
            if (cboNumQuestions.SelectedIndex == -1)
                return false;
            if (rbEasy.IsChecked != true && rbMedium.IsChecked != true && rbHard.IsChecked != true)
                return false;

            return true;
        }
    }
}
