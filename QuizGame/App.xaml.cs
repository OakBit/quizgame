﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace QuizGame
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public string UserName {get; set; }
        public int NumQuestions { get; set; }
        public int Difficulty { get; set; }

        public ObservableCollection<Question> lQuizQuestions;

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            EntryWindow mainWindow = new EntryWindow();
            mainWindow.Show();
        }
        public static new App Current
        {
            get
            {
                return Application.Current as App;
            }
        }
    }
}
