﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace QuizGame
{
    /// <summary>
    /// Interaction logic for ResultWindow.xaml
    /// </summary>
    public partial class ResultWindow : Window
    {
        /*********************************************
        PROPERTIES
        *********************************************/
        private int numCorrect = 0;

        /*********************************************
        LOAD
        *********************************************/
        public ResultWindow()
        {
            InitializeComponent();

            this.Loaded += ResultWindow_Loaded;
        }

        private void ResultWindow_Loaded(object sender, RoutedEventArgs e)
        {
            LoadComponents();

            this.DataContext = this;
        }

        private void LoadComponents()
        {
            //Check the question answers and then add them to the panel
            for(int i = 0; i < App.Current.lQuizQuestions.Count; ++i)
            {
                StackPanel spOuter = new StackPanel();
                spOuter.Orientation = Orientation.Horizontal;
                spOuter.MinHeight = 50;

                Image pic = new Image();
                pic.Width = 50;
                pic.Height = 50;
                BitmapImage bi = new BitmapImage();

                StackPanel spInner = new StackPanel();
                spInner.Orientation = Orientation.Vertical;
                spInner.Margin = new Thickness(5);
                spInner.HorizontalAlignment = HorizontalAlignment.Left;

                TextBlock question = new TextBlock();
                question.Text = "#"+ (i+1) + "  \nQuestion : "+ App.Current.lQuizQuestions[i].ToString();
                question.FontSize = 15;

                TextBlock guess = new TextBlock();
                guess.Text = "Your Answer : " + App.Current.lQuizQuestions[i].SavedGuess;
                guess.FontSize = 15;

                TextBlock answer = new TextBlock();
                answer.Text = "Actual Answer : " + App.Current.lQuizQuestions[i].Answer;
                answer.FontSize = 15;
                answer.Foreground = Brushes.Green;

                spInner.Children.Add(question);
                spInner.Children.Add(guess);


                if (App.Current.lQuizQuestions[i].CheckGuess())
                {
                    numCorrect += 1;
                    guess.Foreground = Brushes.Green;

                    // Create source.
                    // BitmapImage.UriSource must be in a BeginInit/EndInit block.
                    bi.BeginInit();
                    bi.UriSource = new Uri("../../Images/GreenCheckmark.png", UriKind.Relative);
                    bi.CacheOption = BitmapCacheOption.OnLoad;
                    bi.EndInit();
                    pic.Source = bi;
                }
                else
                {
                    guess.Foreground = Brushes.Red;
                    spInner.Children.Add(answer);
                    
                    bi.BeginInit();
                    bi.UriSource = new Uri("../../Images/RedX.png", UriKind.Relative);
                    bi.CacheOption = BitmapCacheOption.OnLoad;
                    bi.EndInit();
                    pic.Source = bi;
                }

                spOuter.Children.Add(pic);
                spOuter.Children.Add(spInner);
                

                spQuestions.Children.Add(spOuter);
            }
            gbResults.Header = App.Current.UserName + " scored " + numCorrect + "/" + App.Current.lQuizQuestions.Count;
        }
        /*********************************************
        EVENTS
        *********************************************/
        private void btnRetry_Click(object sender, RoutedEventArgs e)
        {
            EntryWindow restart = new EntryWindow();
            restart.Show();
            this.Close();
        }
    }
}
