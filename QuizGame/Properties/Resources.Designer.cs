﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace QuizGame.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("QuizGame.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Continue.
        /// </summary>
        public static string EntryBtnContinue {
            get {
                return ResourceManager.GetString("EntryBtnContinue", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Difficulty : .
        /// </summary>
        public static string EntryDifficultyLabel {
            get {
                return ResourceManager.GetString("EntryDifficultyLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Welcome to the math quiz, please enter your information before getting started..
        /// </summary>
        public static string EntryHeaderText {
            get {
                return ResourceManager.GetString("EntryHeaderText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Name : .
        /// </summary>
        public static string EntryNameLabel {
            get {
                return ResourceManager.GetString("EntryNameLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Number of Questions : .
        /// </summary>
        public static string EntryNumQuestionsLabel {
            get {
                return ResourceManager.GetString("EntryNumQuestionsLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Easy.
        /// </summary>
        public static string EntryRbEasy {
            get {
                return ResourceManager.GetString("EntryRbEasy", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hard.
        /// </summary>
        public static string EntryRbHard {
            get {
                return ResourceManager.GetString("EntryRbHard", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Medium.
        /// </summary>
        public static string EntryRbMedium {
            get {
                return ResourceManager.GetString("EntryRbMedium", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancel.
        /// </summary>
        public static string MainCancelBtn {
            get {
                return ResourceManager.GetString("MainCancelBtn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Next.
        /// </summary>
        public static string MainNextBtn {
            get {
                return ResourceManager.GetString("MainNextBtn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Previous.
        /// </summary>
        public static string MainPreviousBtn {
            get {
                return ResourceManager.GetString("MainPreviousBtn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Save.
        /// </summary>
        public static string MainSaveBtn {
            get {
                return ResourceManager.GetString("MainSaveBtn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Submit.
        /// </summary>
        public static string MainSubmitBtn {
            get {
                return ResourceManager.GetString("MainSubmitBtn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Retry.
        /// </summary>
        public static string ResultBtnRetry {
            get {
                return ResourceManager.GetString("ResultBtnRetry", resourceCulture);
            }
        }
    }
}
