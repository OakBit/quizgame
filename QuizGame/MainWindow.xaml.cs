﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace QuizGame
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        /*********************************************
        PROPERTIES
        *********************************************/
        public string GBProp
        {
            get { return (string)GetValue(GBPropProperty); }
            set { SetValue(GBPropProperty, value); }
        }

        // Using a DependencyProperty as the backing store for GBProp.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty GBPropProperty =
            DependencyProperty.Register("GBProp", typeof(string), typeof(MainWindow), new PropertyMetadata(""));



        public string TBQuestionProp
        {
            get { return (string)GetValue(TBQuestionPropProperty); }
            set { SetValue(TBQuestionPropProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TBQuestionProp.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TBQuestionPropProperty =
            DependencyProperty.Register("TBQuestionProp", typeof(string), typeof(MainWindow), new PropertyMetadata(""));



        public Brush IsSavedProp
        {
            get { return (Brush)GetValue(IsSavedPropProperty); }
            set { SetValue(IsSavedPropProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsSavedProp.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsSavedPropProperty =
            DependencyProperty.Register("IsSavedProp", typeof(Brush), typeof(MainWindow), new PropertyMetadata(Brushes.Black));


        /*********************************************
        LOAD
        *********************************************/
        public MainWindow()
        {
            InitializeComponent();

            this.Loaded += MainWindow_Loaded;
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            LoadComponents();

            this.DataContext = this;
            //Start the user off on the first question
            lbQuestions.SelectedIndex = 0;
            var test = lbQuestions.SelectedValue;

        }

        void LoadComponents()
        {

            this.Title = App.Current.UserName + "'s Quiz";

            tbHeaderInfo.Text = "Quiz Settings :    Difficulty - " + (Diff)App.Current.Difficulty + ",  # Questions - " + App.Current.NumQuestions;

            App.Current.lQuizQuestions = new ObservableCollection<Question>();
            for(int i = 0; i < App.Current.NumQuestions; ++i)
            {
                App.Current.lQuizQuestions.Add(new Question(App.Current.Difficulty));
            }
            lbQuestions.ItemsSource = App.Current.lQuizQuestions;
            lbQuestions.FontSize = 15;

            pbQuizProgress.Maximum = lbQuestions.Items.Count;
        }

        /*********************************************
        EVENTS
        *********************************************/
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            Save();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            CancelSave();
        }
        private void lbQuestions_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBox lbQuest = (ListBox)sender;
            GBProp = "Question " + (lbQuest.SelectedIndex + 1);
            TBQuestionProp = lbQuest.SelectedValue.ToString();
            if (App.Current.lQuizQuestions[lbQuestions.SelectedIndex].IsSaved)
            {
                tbAnswer.Text = "" + App.Current.lQuizQuestions[lbQuestions.SelectedIndex].SavedGuess;
                IsSavedProp = Brushes.Green;
            }
            else
            {
                tbAnswer.Text = "";
                IsSavedProp = Brushes.Black;
            }
        }

        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            //Check that the user completed all if not ask if they want to continue anyway
            if(pbQuizProgress.Value < lbQuestions.Items.Count)
            {
                if(MessageBox.Show("There are unanswered questions, Continue anyway?", "Continue?", MessageBoxButton.YesNo) == MessageBoxResult.No)
                    return;
            }
            ResultWindow showResults = new ResultWindow();
            showResults.Show();
            this.Close();
        }

        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            if (lbQuestions.SelectedIndex != lbQuestions.Items.Count)
                lbQuestions.SelectedIndex += 1;

            if (App.Current.lQuizQuestions[lbQuestions.SelectedIndex].IsSaved)
                tbAnswer.Text = "" + App.Current.lQuizQuestions[lbQuestions.SelectedIndex].SavedGuess;
            else
                tbAnswer.Text = "";
            tbAnswer.Focus();
        }

        private void btnPrevious_Click(object sender, RoutedEventArgs e)
        {
            if (lbQuestions.SelectedIndex != 0)
                lbQuestions.SelectedIndex -= 1;

            if (App.Current.lQuizQuestions[lbQuestions.SelectedIndex].IsSaved)
                tbAnswer.Text = "" + App.Current.lQuizQuestions[lbQuestions.SelectedIndex].SavedGuess;
            else
                tbAnswer.Text = "";
            tbAnswer.Focus();
        }


        private void tbAnswer_TextChanged(object sender, TextChangedEventArgs e)
        {
            //If its changed it will no longer be saved if it was
            IsSavedProp = Brushes.Black;
        }

        /*********************************************
        UTILITY
        *********************************************/
        private void Save()
        {
            int ans;
            if (int.TryParse(tbAnswer.Text, out ans))
            {
                if (!App.Current.lQuizQuestions[lbQuestions.SelectedIndex].IsSaved)
                    pbQuizProgress.Value += 1;
                App.Current.lQuizQuestions[lbQuestions.SelectedIndex].Save(ans);
                IsSavedProp = Brushes.Green;
            }
            else
            {
                MessageBox.Show("Invalid format, answer must be integer");
                IsSavedProp = Brushes.Black;
            }
        }

        private void CancelSave()
        {
            App.Current.lQuizQuestions[lbQuestions.SelectedIndex].Cancel();
            pbQuizProgress.Value -= 1;
            IsSavedProp = Brushes.Black;
        }
    }
}
